#ifndef CARD_H_
#define CARD_H_

#include "opencv2/opencv.hpp"
#include <opencv2/nonfree/features2d.hpp>
enum CardType{
	AS = 'A',
	DOIS = '2',
	TRES = '3',
	QUATRO = '4',
	CINCO = '5',
	SEIS = '6',
	SETE = '7',
	DAMA = 'D',
	VALET = 'V',
	REI = 'R'
};

enum CardSuit{
	ESPADAS = 'E',
	COPAS = 'C',
	PAUS = 'P',
	OUROS = 'O'
};

class Card{
public:
	Card();
	Card(std::string, CardType, CardSuit, cv::Mat);
	std::string getName();
	std::string getShortName();
	unsigned int getValue();
	cv::Mat getCard();
	CardSuit getSuit();
	cv::Mat getDescriptor();
private:
	std::string name;
	CardType type;
	CardSuit suit;
	
	cv::Mat cardImage;
	std::vector<cv::KeyPoint> keyPoints;
	cv::Mat descriptors;
};

#endif
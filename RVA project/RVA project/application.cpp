#include "aplication.h"

Aplication::Aplication(){
	camera = cv::VideoCapture(0); 
	if (!camera.isOpened())
		std::cout << "Fail in camera initialization" << std::endl;

	//table = Table();

	cv::namedWindow("Camera", CV_WINDOW_AUTOSIZE);
}

void Aplication::start(){

	while (camera.read(currentCameraFrame)){
		update();

		cv::waitKey(30); // Wait for a keystroke in the window
	}
}

void Aplication::update(){

	table.update(currentCameraFrame);	

	imshow("Camera", currentCameraFrame);
}
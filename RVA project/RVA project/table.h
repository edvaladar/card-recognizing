#ifndef TABLE_H_
#define TABLE_H_

#include "deck.h"


class Table{
public:
	Table();
	void setTrunfo(Card* cardImage);
	void update(cv::Mat &currentCameraFrame);
	void findCard(cv::Mat homography, unsigned int itr);

private:
	
	std::vector<std::vector<cv::Point>> getContourOfCardsOnTable(cv::Mat image);
	std::vector<cv::Point2f> getCardCorners(std::vector<cv::Point> card);
	cv::Mat getCardHomography(cv::Mat currentCameraFrame, std::vector<cv::Point> card, std::vector<cv::Point2f> corners);
	void loadCardsOnTable(cv::Mat currentCameraFrame);
	void analizeCurrentTable(cv::Mat &currentCameraFrame);

	Deck deck;
	Card *trunfo;
	std::pair<Card*, std::vector<cv::Point>> turnFirstCard;

	std::vector<std::pair<Card*, std::vector<cv::Point>>> currentCards;
	std::vector<cv::Mat> tableCardsHomographys;
	std::vector<std::vector<cv::Point> > contours_poly;


	std::vector<std::vector<std::string>> handsLog;
};

#endif
#include "card.h"


Card::Card(){
}

Card::Card(std::string _name, CardType _cardType, CardSuit _cardSuit, cv::Mat _cardImage){
	name = _name;
	type = _cardType;
	suit = _cardSuit;
	cardImage = _cardImage;

	cv::SiftFeatureDetector detector(400);
	detector.detect(cardImage, keyPoints);
	cv::SiftDescriptorExtractor extractor;
	extractor.compute(cardImage, keyPoints, descriptors);
}

std::string Card::getName(){
	return name;
}
std::string Card::getShortName(){
	return ""+type+suit;
}
unsigned int Card::getValue(){
	switch (type)
	{
	case AS:
		return 11;
		break;
	case DOIS:
		return 2;
		break;
	case TRES:
		return 3;
		break;
	case QUATRO:
		return 4;
		break;
	case CINCO:
		return 5;
		break;
	case SEIS:
		return 6;
		break;
	case SETE:
		return 10;
		break;
	case DAMA:
		return 7;
		break;
	case VALET:
		return 8;
		break;
	case REI:
		return 9;
		break;
	default:
		break;
	}
}

CardSuit Card::getSuit(){
	return suit;
}
cv::Mat Card::getCard(){
	return cardImage;
}

cv::Mat Card::getDescriptor(){
	return descriptors;
}
﻿#include "deck.h"
#include <thread>


cv::Mat preprocess2(cv::Mat _image){

	cv::Mat gray, blur, thresh, blur_thresh;
	cvtColor(_image, gray, cv::COLOR_BGR2GRAY);
	GaussianBlur(gray, blur, cv::Size(5, 5), 2);
	adaptiveThreshold(blur, thresh, 255, 1, 1, 11, 1);
	GaussianBlur(thresh, blur_thresh, cv::Size(5, 5), 5);

	return blur_thresh;
}

void Deck::loadCard(std::pair<std::string, std::string> cardName){

	cv::Mat src = cv::imread("deck/" + cardName.first + ".jpg");

	if (src.empty())
		return;

	cards.push_back(new Card(cardName.first, (CardType) (cardName.second[0]), (CardSuit) (cardName.second[1]), src));

	std::cout << cardName.first << std::endl;
}

void threadCallBack(Deck *deck, std::pair<std::string, std::string> cardName){
	deck->loadCard(cardName);
}

Deck::Deck(){

}

void Deck::generateDeck(){

	std::vector<std::thread> threads;

	std::cout << "A gerar o deck (criar, preprocessar e determinar keypoints)" << std::endl;
	std::vector<std::pair<std::string, std::string>> cardName;
	
	cardName.push_back(std::make_pair("As Espadas", "AE"));	    cardName.push_back(std::make_pair("As Copas", "AC"));     cardName.push_back(std::make_pair("As Paus", "AP"));      cardName.push_back(std::make_pair("As Ouros", "AO"));
	cardName.push_back(std::make_pair("Dois Espadas", "2E"));	cardName.push_back(std::make_pair("Dois Copas", "2C"));	  cardName.push_back(std::make_pair("Dois Paus", "2P"));	cardName.push_back(std::make_pair("Dois Ouros", "2O"));
	cardName.push_back(std::make_pair("Tres Espadas", "3E"));	cardName.push_back(std::make_pair("Tres Copas", "3C"));	  cardName.push_back(std::make_pair("Tres Paus", "3P"));	cardName.push_back(std::make_pair("Tres Ouros", "3O"));
	cardName.push_back(std::make_pair("Quatro Espadas", "4E"));	cardName.push_back(std::make_pair("Quatro Copas", "4C")); cardName.push_back(std::make_pair("Quatro Paus", "4P"));	cardName.push_back(std::make_pair("Quatro Ouros", "4O"));
	cardName.push_back(std::make_pair("Cinco Espadas", "5E"));	cardName.push_back(std::make_pair("Cinco Copas", "5C"));  cardName.push_back(std::make_pair("Cinco Paus", "5P"));	cardName.push_back(std::make_pair("Cinco Ouros", "5O"));
	cardName.push_back(std::make_pair("Seis Espadas", "6E"));	cardName.push_back(std::make_pair("Seis Copas", "6C"));	  cardName.push_back(std::make_pair("Seis Paus", "6P"));	cardName.push_back(std::make_pair("Seis Ouros", "6O"));
	cardName.push_back(std::make_pair("Sete Espadas", "7E"));	cardName.push_back(std::make_pair("Sete Copas", "7C"));	  cardName.push_back(std::make_pair("Sete Paus", "7P"));	cardName.push_back(std::make_pair("Sete Ouros", "7O"));
	cardName.push_back(std::make_pair("Dama Espadas", "DE"));	cardName.push_back(std::make_pair("Dama Copas", "DC"));	  cardName.push_back(std::make_pair("Dama Paus", "DP"));	cardName.push_back(std::make_pair("Dama Ouros", "DO"));
	cardName.push_back(std::make_pair("Valet Espadas", "VE"));	cardName.push_back(std::make_pair("Valet Copas", "VC"));  cardName.push_back(std::make_pair("Valet Paus", "VP"));	cardName.push_back(std::make_pair("Valet Ouros", "VO"));
	cardName.push_back(std::make_pair("Rei Espadas", "RE"));	cardName.push_back(std::make_pair("Rei Copas", "RC"));	  cardName.push_back(std::make_pair("Rei Paus", "RP"));	    cardName.push_back(std::make_pair("Rei Ouros", "RO"));

	unsigned int itr(0);

	for (; itr < cardName.size(); itr++){
		threads.push_back(std::thread(threadCallBack, this, cardName[itr]));
	}

	for (unsigned int i(0); i < threads.size(); i++)
		threads[i].join();

	std::cout << itr << " cartas carregadas." << std::endl;
}

Card* Deck::getCard(std::string _cardName){
	for (unsigned int i(0); i < cards.size(); i++)
		if (cards[i]->getName() == _cardName)
			return cards[i];

	return nullptr;
}

cv::Mat Deck::find(cv::Mat _card){
	bool found = false;

	cv::Mat diff;

	return diff;
}

Card* Deck::findBySIFT(cv::Mat _card){

	// detecting keypoints
	cv::SiftFeatureDetector detector(400);
	std::vector<cv::KeyPoint> keypoints1;
	detector.detect(_card, keypoints1);

	// computing descriptors
	cv::SiftDescriptorExtractor extractor;
	cv::Mat descriptors1;
	extractor.compute(_card, keypoints1, descriptors1);

	// matching descriptors
	

	std::pair<std::string, int> currentPair = std::make_pair("", 0);

	Card *temp = new Card();

	for (unsigned int itr(0); itr < cards.size(); itr++){
		cv::FlannBasedMatcher matcher;
		std::vector<cv::DMatch> matches;
		std::vector<cv::DMatch> goodMatches = std::vector<cv::DMatch>();

		matcher.match(descriptors1, cards[itr]->getDescriptor(), matches);

		for (unsigned int i(0); i < matches.size(); i++)
		{
			if (matches[i].distance < 160){
				goodMatches.push_back(matches[i]);
			}
		}	

		if ((int)goodMatches.size() > currentPair.second){
			currentPair = std::make_pair(cards[itr]->getName(), (int)goodMatches.size());
			temp = (cards[itr]);
		}
	}

	std::cout << currentPair.first << " - " << temp->getValue() << "| Numero de goodmatches --> " << currentPair.second << std::endl;

	return temp;
}
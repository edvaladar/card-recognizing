#ifndef APLICATION_H_
#define APLICATION_H_

#include "table.h"

class Aplication{
public:
	Aplication();
	void start();
private:
	void update();
	Table table;

	cv::VideoCapture camera;
	cv::Mat currentCameraFrame;
};

#endif
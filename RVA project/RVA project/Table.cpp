#include "table.h"
#include <thread>
#include <Windows.h>

bool compareContours(std::vector<cv::Point> a1, std::vector<cv::Point> a2){
	double a1Area = fabs(contourArea(cv::Mat(a1)));
	double a2Area = fabs(contourArea(cv::Mat(a2)));
	return a1Area > a2Area;
}

bool sortCorners(std::vector<cv::Point2f>& corners, cv::Point2f center)
{
	std::vector<cv::Point2f> top, bot;

	for (int i = 0; i < corners.size(); i++)
	{
		if (corners[i].y < center.y)
			top.push_back(corners[i]);
		else
			bot.push_back(corners[i]);
	}

	if (!(top.size() == 2 && bot.size() == 2))
		return false;

	cv::Point2f tl = top[0].x > top[1].x ? top[1] : top[0];
	cv::Point2f tr = top[0].x > top[1].x ? top[0] : top[1];
	cv::Point2f bl = bot[0].x > bot[1].x ? bot[1] : bot[0];
	cv::Point2f br = bot[0].x > bot[1].x ? bot[0] : bot[1];

	corners.clear();
	corners.push_back(tl);
	corners.push_back(tr);
	corners.push_back(br);
	corners.push_back(bl);

	return true;
}

double distanceBetweenPoints(cv::Point2f p1, cv::Point2f p2){

	return sqrt(pow(abs(p1.x - p2.x), 2) + pow(abs(p1.y - p2.y), 2));
}

void threadCallBack(Table* table, cv::Mat homography, unsigned int i){
	table->findCard(homography, i);
	
}

//--------------------------------------------------------------------------------------------------------------------------------------------------

Table::Table(){
	deck = Deck();
	deck.generateDeck();

	trunfo = nullptr;
	turnFirstCard.first = nullptr;

	std::cout << "Identifica a carta de trunfo para a camara." << std::endl;
	system("pause");
}

void Table::setTrunfo(Card* cardImage){
	trunfo = cardImage;
	
	std::cout << "O trunfo escolhido foi: " << trunfo->getName() << std::endl;
}

std::vector<std::vector<cv::Point>> Table::getContourOfCardsOnTable(cv::Mat image){

	std::vector<std::vector<cv::Point>> returnCards;

	cv::Mat gray, blur, thresh, contours;
	std::vector<std::vector<cv::Point>> listOfContours;
	cvtColor(image, gray, cv::COLOR_BGR2GRAY);
	GaussianBlur(gray, blur, cv::Size(1, 1), 1000, 0);
	threshold(blur, thresh, 120, 255, cv::THRESH_BINARY);

	//Save copy of thresh
	contours = thresh;

	findContours(contours, listOfContours, CV_RETR_TREE, CV_CHAIN_APPROX_SIMPLE);
	sort(listOfContours.begin(), listOfContours.end(), compareContours);

	for (unsigned int i(0); i < listOfContours.size(); i++){
		if (arcLength(listOfContours[i], true) > 500){
			returnCards.push_back(listOfContours[i]);
		}
	}

	return returnCards;
}

std::vector<cv::Point2f> Table::getCardCorners(std::vector<cv::Point> card){

	double peri = arcLength(card, true);
	std::vector<cv::Point> approx;
	approxPolyDP(card, approx, 0.02*peri, true);

	cv::RotatedRect rect = minAreaRect(card);

	std::vector<cv::Point2f> corners = std::vector<cv::Point2f>();

	for each (cv::Point2f var in approx)
		corners.push_back(var);

	if (!sortCorners(corners, rect.center))
		return std::vector<cv::Point2f>();

	return corners;
}

cv::Mat Table::getCardHomography(cv::Mat currentCameraFrame, std::vector<cv::Point> card, std::vector<cv::Point2f> corners){
	cv::Mat homography = cv::Mat::zeros(532, 344, CV_8UC3);

	cv::Point2f quads[4];
	if (distanceBetweenPoints(corners[0], corners[1]) > distanceBetweenPoints(corners[1], corners[2])){
		quads[0] = cv::Point(homography.cols, 0);
		quads[1] = cv::Point(homography.cols, homography.rows);
		quads[2] = cv::Point(0, homography.rows);
		quads[3] = cv::Point(0, 0);
	}
	else{
		quads[0] = cv::Point(0, 0);
		quads[1] = cv::Point(homography.cols, 0);
		quads[2] = cv::Point(homography.cols, homography.rows);
		quads[3] = cv::Point(0, homography.rows);
	}

	cv::Point2f temp[4];
	temp[0] = corners[0];
	temp[1] = corners[1];
	temp[2] = corners[2];
	temp[3] = corners[3];

	cv::Mat transform = getPerspectiveTransform(temp, quads);
	warpPerspective(currentCameraFrame, homography, transform, homography.size());

	return homography;
}

void Table::loadCardsOnTable(cv::Mat currentCameraFrame){
	//get contourns of cards on table
	std::vector<std::vector<cv::Point>> cards = getContourOfCardsOnTable(currentCameraFrame);

	//homographys of car on table
	tableCardsHomographys = std::vector<cv::Mat>();

	//contours of cards
	contours_poly = std::vector<std::vector<cv::Point> >(cards.size());

	for (unsigned int i(0); i < cards.size(); i++)
	{
		std::vector<cv::Point2f> corners = getCardCorners(cards[i]);

		if (corners.size() == 0)
			continue;

		//contornos das cartas
		approxPolyDP(cards[i], contours_poly[i], 0.5, true);
		tableCardsHomographys.push_back(getCardHomography(currentCameraFrame, cards[i], corners));
	}

	//load cards object
	currentCards = std::vector<std::pair<Card*, std::vector<cv::Point>>>();

	if (trunfo != nullptr && tableCardsHomographys.size() > 0)
		std::cout << "\n================  " << tableCardsHomographys.size() << " cartas na mesa =====================" << std::endl;

	std::vector<std::thread> threads;
	for (unsigned int i(0); i < tableCardsHomographys.size(); i++){
		threads.push_back(std::thread(threadCallBack, this, tableCardsHomographys[i], i));
	}

	for (unsigned int i(0); i < threads.size(); i++)
		threads[i].join();
}

void Table::findCard(cv::Mat homography, unsigned int itr){
	currentCards.push_back(std::make_pair(deck.findBySIFT(homography), contours_poly[itr]));
}

void Table::analizeCurrentTable(cv::Mat &currentCameraFrame){
	if (currentCards.size() > 0){

		std::vector<std::vector<cv::Point>> temp;

		if (turnFirstCard.first == nullptr){

			if (currentCards.size() > 1){
				std::cout << "A primeira carta do turno n�o foi identificada corretameente." << std::endl;
				return;
			}

			turnFirstCard = currentCards[0];

			return;
		}
		else{
			temp.push_back(currentCards[0].second);
			drawContours(currentCameraFrame, temp, 0, cv::Scalar(0, 255, 0), 2, 8, std::vector<cv::Vec4i>(), 0, cv::Point());
		}

		Card * highCard = currentCards[0].first;
		unsigned int itr = 0;

		for (unsigned int i(0); i < currentCards.size(); i++)
			if (currentCards[i].first->getSuit() == trunfo->getSuit()){ //carta jogada � trunfo
				if (highCard->getSuit() == trunfo->getSuit()){ //carta mais alta currente � trunfo
					if (currentCards[i].first->getValue() > highCard->getValue()){
						highCard = currentCards[i].first;
						itr = i;
					}
				}
				else{ // carta mais alta currente n�o � trunfo
					highCard = currentCards[i].first;
					itr = i;
				}
			}
			else{ //carta jogada n�o � trunfo
				if (highCard->getSuit() == trunfo->getSuit()){ //carta mais alta currente � trunfo
					continue;
				}
				else{ // carta mais alta currente n�o � trunfo
					if (currentCards[i].first->getValue() > highCard->getValue()){
						highCard = currentCards[i].first;
						itr = i;
					}
				}
			}

		
		temp[0] = (currentCards[itr].second);
		
		drawContours(currentCameraFrame, temp, 0, cv::Scalar(0, 0, 255), 2, 8, std::vector<cv::Vec4i>(), 0, cv::Point());

		cv::putText(currentCameraFrame, "Vencedora", cv::Size(temp[0][0].x, temp[0][0].y), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(20, 20, 20), 2);
		cv::putText(currentCameraFrame, "Vencedora", cv::Size(temp[0][0].x-2, temp[0][0].y-2), cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(255,255,255), 2);
	}
}

void Table::update(cv::Mat &currentCameraFrame){

	if (trunfo == nullptr){
		
		loadCardsOnTable(currentCameraFrame);

		if (currentCards.size() > 0){
			setTrunfo(currentCards[0].first);
			std::cout << "Retire o trunfo." << std::endl;
			system("pause");
			return;
		}

	}
	else{
		loadCardsOnTable(currentCameraFrame);
		analizeCurrentTable(currentCameraFrame);
	}
}
#ifndef DECK_H_
#define DECK_H_

#include "card.h"
#include <vector>
#include <Windows.h>
#include <math.h>


class Deck{
public:
	Deck();
	void generateDeck();
	Card* getCard(std::string);
	cv::Mat find(cv::Mat _card);
	Card* findBySIFT(cv::Mat _card);
	void loadCard(std::pair<std::string, std::string> cardName);
private:
	std::vector<Card*> cards;
};

#endif